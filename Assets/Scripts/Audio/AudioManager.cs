﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;

    public static AudioManager instance;
    public AudioSource music;

    public AudioReverbFilter reverbFilter;
    public AudioLowPassFilter lowPassFilter;

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);


        reverbFilter = GetComponentInChildren<AudioReverbFilter>();
        lowPassFilter = GetComponentInChildren<AudioLowPassFilter>();

        //verbFilter.reverbPreset = AudioReverbPreset.Plain;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogError("Missing sound: " + name);
            return;
        }
        s.source.Play();
    }

    public void StopSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogError("Missing sound: " + name);
            return;
        }

        //TODO: gradually fade out music rather then suddenly stopping music

        s.source.Stop();
    }

    public void ChangeMusic(string name)
    {
        if (name != null)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogError("Missing sound: " + name);
                return;
            }
            music.clip = s.clip;
            music.Play();
        }
    }

    public void SetAudioReverbPreset(AudioReverbPreset preset)
    {
        reverbFilter.reverbPreset = preset;
    }

}
