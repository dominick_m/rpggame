﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {

    public string name;

    public AudioClip clip;
    [HideInInspector]
    public AudioSource source;

    [Space]

    [Range(0f, 1f)]
    public float volume = 1f;
    [Range(-3, 3f)]
    public float pitch = 1f;

    public bool loop = false;

}
