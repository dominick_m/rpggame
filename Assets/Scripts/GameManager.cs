﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public GameObject player;

	// Use this for initialization
	void Awake () {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        //player = GameObject.FindGameObjectWithTag("Player");
	}

    void Update()
    {
        if (player == null && GameObject.FindGameObjectWithTag("Player") != null)
            Setup();
        
    }

    void Setup()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene sc, LoadSceneMode mode)
    {
        player = GameObject.FindGameObjectWithTag("Player");
        float fadeSpeed = GetComponent<ScreenFade>().BeginFade(-1);
        Invoke("EnableCanPause", fadeSpeed);
        Invoke("EnablePlayerMove", fadeSpeed);
    }

    void EnableCanPause()
    {
        PauseManager.CanPause = true;
    }

    public void EnablePlayerMove()
    {
        if(player != null)
            player.GetComponent<OverworldPlayerController>().enabled = true;
    }

    public void DisablePlayerMove()
    {
        if (player != null)
        {
            player.GetComponent<OverworldPlayerController>().anim.SetBool("PlayerMoving", false);
            player.GetComponent<OverworldPlayerController>().isMoving = false;
            player.GetComponent<OverworldPlayerController>().enabled = false;
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
