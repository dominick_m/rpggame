﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class TriggerSceneSwitch : MonoBehaviour {

    public string targetScene;
    public bool changeMusic = false;
    public string musicChangeSound;

    void OnTriggerEnter2D(Collider2D collision)
    {

        FindObjectOfType<GameManager>().DisablePlayerMove();
        StartLoad();
    }


    //Run this first
    public void StartLoad()
    {
        PauseManager.CanPause = false;
        float fadeTime = FindObjectOfType<ScreenFade>().GetComponent<ScreenFade>().BeginFade(1);
        Invoke("LoadScene", fadeTime);

    }

    void LoadScene()
    {

        SceneManager.LoadScene(targetScene);
        if(musicChangeSound != null && changeMusic == true)
            FindObjectOfType<AudioManager>().ChangeMusic(musicChangeSound);
    }
}
