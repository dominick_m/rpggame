﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class OverworldPlayerController : MonoBehaviour {
    
    //Using Rigidbodies to move the player rather than changing transform.position
    //So that it will be possible to register collision
    Rigidbody2D rb2d;

    public Animator anim;
    public bool isMoving;
    public Vector2 lastMove;

    [Range(3,20)]
    public float speed = 4f;
    public float currSpeed;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        if(GetComponent<Animator>() != null)
            anim = GetComponent<Animator>();
    }

    void Update () {

        //Left shift sprint
        if (Input.GetAxisRaw("Sprint") == 1 || Input.GetKey(KeyCode.LeftShift))
            currSpeed = speed * 2;
        else
            currSpeed = speed;

        //Update Move
        float xInp = Input.GetAxis("Horizontal") * currSpeed;
        float yInp = Input.GetAxis("Vertical") * currSpeed;

        rb2d.velocity = new Vector2(xInp, yInp);

        //Idle animation directions
        if (new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) != Vector2.zero)
            lastMove = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (rb2d.velocity != Vector2.zero)
            isMoving = true;
        else
            isMoving = false;

        //Animation parameters
        if (anim != null && !PauseManager.GamePaused)
        {
            anim.SetFloat("MoveX", xInp);
            anim.SetFloat("MoveY", yInp);

            anim.SetFloat("LastMoveX", lastMove.x);
            anim.SetFloat("LastMoveY", lastMove.y);

            anim.SetBool("PlayerMoving", isMoving);
        }

	}
}
