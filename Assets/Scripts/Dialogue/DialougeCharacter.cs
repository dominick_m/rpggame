﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Create > Character
[CreateAssetMenu(fileName = "New Dialouge Character", menuName = "Dialogue/Character")]
public class DialougeCharacter : ScriptableObject {
    //Stores all character's mood sprites and name to be referenced in Dialouge

    public new string name;
    //public string description;

    public Sprite neutralProfile;
    public Sprite happyProfile;
    public Sprite sadProfile;
    public Sprite angeryProfile;
    public Sprite questioningProfile;

}
