﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialougeManager : MonoBehaviour {

    public Queue<Dialouge> sentences;

    public TMP_Text nameText;
    public TMP_Text sentenceText;
    public GameObject panel;
    public Image characterProfile;
    public string talkSound;

    Dialouge currDialouge;
    public DialougeCharacter leftCharacter;
    public DialougeCharacter rightCharacter;

    public Sprite Blank;

    bool isInDialouge = false;
    public bool isTyping = false;

    AudioManager am;
    Canvas canvas;
    Animator anim;

	// Use this for initialization
	void Start () {
        sentences = new Queue<Dialouge>();
        if(panel != null)
            if(panel.GetComponent<Animator>() != null)
                anim = panel.GetComponent<Animator>();
        if (FindObjectOfType<AudioManager>() != null)
            am = FindObjectOfType<AudioManager>();
        canvas = GetComponent<Canvas>();
    }

    void Update()
    {
        if ((Input.GetButtonDown("Submit") || Input.GetMouseButtonDown(0)) && isInDialouge && !isTyping)
            DisplayNextSentence();
        //else if(isTyping)
        //sentenceText.text = currDialouge.name;

        if (panel == null && GameObject.FindGameObjectWithTag("DialoguePanel"))
        {
            panel = GameObject.FindGameObjectWithTag("DialoguePanel");
            Setup();
        }

        if (canvas.worldCamera == null)
            canvas.worldCamera = FindObjectOfType<Camera>();
    }

    void Setup()
    {
        nameText = GameObject.Find("Panel/NameText").GetComponent<TMP_Text>();
        sentenceText = GameObject.Find("Panel/SentenceText").GetComponent<TMP_Text>();
        characterProfile = GameObject.Find("Panel/Image").GetComponent<Image>();
        if (panel.GetComponent<Animator>() != null)
            anim = panel.GetComponent<Animator>();
        if (FindObjectOfType<AudioManager>() != null)
            am = FindObjectOfType<AudioManager>();
    }

    public void StartDialogue(Dialouge[] dialogue)
    {
        PauseManager.CanPause = false;
        sentences.Clear();

        foreach (Dialouge d in dialogue)
            sentences.Enqueue(d);

        FindObjectOfType<GameManager>().DisablePlayerMove();
        FindObjectOfType<GameManager>().player.GetComponent<PlayerFacing>().enabled = false;
        characterProfile.color = Color.white;

        if(anim != null)
            anim.SetBool("isOpen", true);

        isInDialouge = true;
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialouge();
            return;
        }

        Dialouge currDialouge = sentences.Dequeue();

        StartCoroutine("TypeSentence", currDialouge.sentence);
        nameText.text = currDialouge.name;
        characterProfile.sprite = currDialouge.characterProfile;
    }

    IEnumerator TypeSentence(string s)
    {
        isTyping = true;
        StartCoroutine("TalkSentence");
        sentenceText.text = "";

        foreach (char c in s.ToCharArray())
        {
            sentenceText.text += c;



            if (sentenceText.text == s)
                isTyping = false;
            yield return null;
        }
    }

    IEnumerator TalkSentence()
    {
        while (isTyping)
        {
            if (talkSound == null)
                am.PlaySound("TalkDefault");
            else
                am.PlaySound(talkSound);
            yield return new WaitForSeconds(.05f);
        }
    }


    void EndDialouge()
    {
        PauseManager.CanPause = true;

        FindObjectOfType<GameManager>().EnablePlayerMove();
        FindObjectOfType<GameManager>().player.GetComponent<PlayerFacing>().enabled = true;
        //characterProfile.sprite = null;
        //characterProfile.color = Color.clear;
        nameText.text = " ";
        sentenceText.text = " ";

        if(anim != null)
            anim.SetBool("isOpen", false);

        isInDialouge = false;
    }
}
