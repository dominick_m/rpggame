﻿using UnityEngine;

[System.Serializable]
public class Dialouge {
    public enum Mood { Neutral, Happy, Sad, Angery, Questioning };
    public Mood mood;

    public DialougeCharacter character;

    [HideInInspector]
    public string name;
    [HideInInspector]
    public Sprite characterProfile;

    [TextArea(3,10)]
    public string sentence;

    public void Setup()
    {
        name = character.name;

        switch (mood)
        {
            case Mood.Neutral:
                characterProfile = character.neutralProfile;
                break;

            case Mood.Happy:
                characterProfile = character.happyProfile;
                break;

            case Mood.Sad:
                characterProfile = character.sadProfile;
                break;

            case Mood.Angery:
                characterProfile = character.angeryProfile;
                break;

            case Mood.Questioning:
                characterProfile = character.questioningProfile;
                break;

            default:
                characterProfile = character.neutralProfile;
                break;
        }
    }

    /*  Take character scriptable object
     *  Use character scriptable object's information like name, profile
     *   profile.angry, profile.sad, profile.netural
     */


}
