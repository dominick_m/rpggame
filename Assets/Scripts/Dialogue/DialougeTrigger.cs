﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialougeTrigger : MonoBehaviour {

    public Dialouge[] dialogue;

    public void TriggerDialogue()
    {
        foreach (Dialouge d in dialogue)
            d.Setup();

        FindObjectOfType<DialougeManager>().StartDialogue(dialogue);
    }
}
