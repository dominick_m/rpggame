﻿using UnityEngine.EventSystems;
using UnityEngine;

public class MenuCursor : MonoBehaviour {

    public EventSystem es;
    public GameObject currSelected;
    RectTransform rt;

    GameObject sideSelect;
    GameObject topSelect;

    public float offset;

    void Start()
    {
        if (es == null)
            Debug.LogError(gameObject.name + " doesn't have an eventsystem attached!");
        rt = GetComponent<RectTransform>();
        currSelected = es.firstSelectedGameObject;
    }

    void Update()
    {
        if (es.currentSelectedGameObject != null)
        {
            currSelected = es.currentSelectedGameObject.gameObject;
            RectTransform selectedRt = currSelected.GetComponent<RectTransform>();
            if (es.currentSelectedGameObject.GetComponent<TopPauseButton>() != null)
                rt.anchoredPosition = new Vector2(selectedRt.anchoredPosition.x, selectedRt.anchoredPosition.y + 30f);
            else
                rt.anchoredPosition = new Vector2(selectedRt.anchoredPosition.x - (selectedRt.rect.width / 2) - 30f, selectedRt.anchoredPosition.y);
        }
    }

}
