﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Battle Character", menuName = "Battle/Character")]
public class BattleCharacter : ScriptableObject {

    public new string name;

    public Stats stats;

    public Weapon weapon;
    public Armor armor;

    public Spell[] spells = new Spell[4];


}
