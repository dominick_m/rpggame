﻿using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class ChargeMinigame : Minigame {

    public Slider slider;
    public float speed = 0.7f;
    public bool isSliding = true;
    float min = 0;
    float max = 1;
    public float t;

    float currentLerpTime = 0f;
    bool fwd = true;

    public ChargeMinigame()
    {
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isSliding)
        {

            //t += speed * Time.deltaTime;
            currentLerpTime += Time.deltaTime;
            //else if (currentLerpTime < 0)
            //    speed = -speed;

            t = currentLerpTime / speed;
            if (fwd)
                t = t * t * t;
            else
                t = -(t * t * t) + 1;

            slider.value = Mathf.Lerp(min, max, t);

            if ((t > 1.0f && fwd) || (t < 0.0f && !fwd))
            {
                fwd = !fwd;
                //float temp = max;
                //max = min;
                //min = temp;
                t = 0;
                currentLerpTime = 0f;
            }
        }

        if (Input.GetButtonDown("Jump"))
            Debug.Log(CheckGrade(StopCharge()));
	}

    float StopCharge()
    {
        isSliding = false;
        return slider.value;
    }

    int CheckGrade(float g)
    {
        return 1;
    }
}
