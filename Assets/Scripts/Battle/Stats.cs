﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats {

    public int ATK = 0;
    public int DEF = 0;
    public int SPD = 0;
    public int LUK = 0;
    public int VIT = 0;
    public int WIS = 0;

    public int level = 1;

    public int swordProf = 1;
    public int staffProf = 1;
    public int bowProf = 1;

}
