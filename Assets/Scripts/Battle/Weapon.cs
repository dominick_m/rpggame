﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Battle/Weapon")]
public class Weapon : ScriptableObject {

    public new string name;
    [TextArea(3,5)]
    public string description;

    public enum WeaponType { sword, staff, bow }

    public Sprite sprite;
    public WeaponType weapontype;

    public Stats statChange;

    //Stats for attributes:
    //0 = no change
    //<0 = positive change
    //>0 = negative change

    //Prof: required prof to wield weapon
    //sword prof 1 = requires character to be sword prof 1 or above
    //cannot be negative
}
