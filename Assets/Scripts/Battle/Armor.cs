﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armor", menuName = "Battle/Armor")]
public class Armor : ScriptableObject
{
    public new string name;
    [TextArea(2, 4)]
    public string description;

    public Sprite sprite;
    public Stats statChange;

}
