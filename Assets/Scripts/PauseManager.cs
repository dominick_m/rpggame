﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

    public static bool GamePaused;
    public static bool CanPause;
    public bool pauseMenuUp;
    public GameObject pauseMenu;


    void Update()
    {
        if (Input.GetButtonDown("Pause") && !GamePaused && CanPause)
            GamePaused = true;
        else if (Input.GetButtonDown("Pause") && GamePaused && CanPause)
            GamePaused = false;


        if (GamePaused && !pauseMenuUp && CanPause)
            Pause();
        else if (!GamePaused && pauseMenuUp && CanPause)
            UnPause();
    }



    public void Pause()
    {
        GamePaused = true;
        pauseMenuUp = true;
        pauseMenu.SetActive(true);
        FindObjectOfType<AudioManager>().lowPassFilter.enabled = true;
        //GamePaused = true;
        Time.timeScale = 0f;
    }

    public void UnPause()
    {
        GamePaused = false;
        pauseMenuUp = false;
        pauseMenu.SetActive(false);
        FindObjectOfType<AudioManager>().lowPassFilter.enabled = false;
        Time.timeScale = 1f;
        //GamePaused = false;

    }

}
