﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sign : MonoBehaviour {

    Animator anim;

    void Start()
    {
        if (GetComponent<Animator>() == null)
            Debug.LogWarning("Sign " + gameObject.name + " does not have an animator!");
        else
            anim = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
            anim.SetBool("inRange", true);
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            anim.SetBool("inRange", false);
    }

}
