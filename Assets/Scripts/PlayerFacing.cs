﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFacing : MonoBehaviour {


    OverworldPlayerController pc;
    RaycastHit2D hit;

	// Use this for initialization
	void Start () {
        pc = GetComponent<OverworldPlayerController>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 a = transform.position;
        Vector2 b = pc.lastMove;

        if (Input.GetButtonDown("Select"))
        {
            GetComponent<Collider2D>().enabled = false;
            //Clamp distance
            hit = Physics2D.Raycast(a,b, 2f, 1 << 8);
            GetComponent<Collider2D>().enabled = true;
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.GetComponent<DialougeTrigger>() != null)
                    hit.collider.gameObject.GetComponent<DialougeTrigger>().TriggerDialogue();
                Debug.Log("Raycast hit " + hit.collider.gameObject.name);
            }
        }




        Debug.DrawRay(a,b);
		
	}
}
