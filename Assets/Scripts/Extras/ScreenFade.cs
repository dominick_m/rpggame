﻿using UnityEngine;

//https://www.youtube.com/watch?v=0HwZQt94uHQ
public class ScreenFade : MonoBehaviour {


    public Texture2D fadeOutTexture;    //Texture that will be overlayed. Can be a black image or a loading graphic
    public float fadeSpeed = 0.8f;

    int drawDepth = -1000; //Texture's draw order. Lower numbers means it will render on top
    float alpha = 1.0f;
    int fadeDir = -1;   //Direction to fade in -1 or 1


    void OnGUI()
    {
        //Fades in or out the alpha value using direction, speed, and time.deltatime
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        //Draws the texture
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);

    }

    public float BeginFade(int direction)
    {
        fadeDir = direction;
        return (fadeSpeed);
    }
}
