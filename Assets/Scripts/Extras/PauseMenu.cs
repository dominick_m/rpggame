﻿using UnityEngine.EventSystems;
using UnityEngine;

public class PauseMenu : Menu {

    //To be set on the pausemenu panel.

    public GameObject[] menuScreens;
    public int currMenuScreen;
    //public int selectedMenuScreen;

    //public new EventSystem es;

    PauseMenu instance;

	// Use this for initialization
	new void Start () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);

        base.Start();
        ChangeMenuScreen(0);
	}

    new void Update()
    {

        base.Update();
        //ChangeMenuScreen(selectedMenuScreen);
    }


    public void ChangeMenuScreen(int index)
    {
        currMenuScreen = index;
        foreach (GameObject m in menuScreens)
        {
            m.SetActive(false);
        }
        menuScreens[currMenuScreen].SetActive(true);
    }

    void OnEnable()
    {

        ChangeMenuScreen(0);
        es.SetSelectedGameObject(FindObjectOfType<EventSystem>().firstSelectedGameObject);
        GetComponentInChildren<MenuCursor>().currSelected = es.firstSelectedGameObject;
    }
}
