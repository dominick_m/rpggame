﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyLoad : MonoBehaviour {

    GameObject instance;

	void Start () {
        if (instance == null)
            instance = gameObject;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }
}
