﻿using UnityEngine.EventSystems;
using UnityEngine;

public class TopPauseButton : MonoBehaviour {

    public int selectNumber = 0;
    EventSystem es;

	// Use this for initialization
	void Start () {
        es = FindObjectOfType<EventSystem>();

    }
	
	// Update is called once per frame
	void Update () {
        if (es.currentSelectedGameObject == gameObject && FindObjectOfType<PauseMenu>().currMenuScreen != selectNumber)
            FindObjectOfType<PauseMenu>().ChangeMenuScreen(selectNumber);
	}
}
