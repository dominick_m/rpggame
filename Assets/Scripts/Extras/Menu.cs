﻿using UnityEngine.EventSystems;
using UnityEngine;

public class Menu : MonoBehaviour {

    public EventSystem es;
    public GameObject lastSelected;
    Canvas canvas;

    public void Start()
    {
        if (es == null)
            Debug.LogError("Menu " + gameObject.name + " eventsystem is not set!!");

        canvas = GetComponent<Canvas>();

        //es = FindObjectOfType<EventSystem>();
        lastSelected = es.currentSelectedGameObject;
    }

    public void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            if (lastSelected.GetComponent<ButtonSound>() != null)
            {
                string soundname = lastSelected.gameObject.GetComponent<ButtonSound>().name;
                FindObjectOfType<AudioManager>().PlaySound(soundname);
            }
            else
                FindObjectOfType<AudioManager>().PlaySound("Menu1");
        }

        if (lastSelected != es.currentSelectedGameObject && es.currentSelectedGameObject != null)
        {
            lastSelected = es.currentSelectedGameObject;
            FindObjectOfType<AudioManager>().PlaySound("Menu2");
        }

        if (canvas.worldCamera == null)
            canvas.worldCamera = FindObjectOfType<Camera>();

        if (es.currentSelectedGameObject == null)
            es.SetSelectedGameObject(lastSelected);
    }

    public void ChangeCurrentSelected(GameObject g)
    {
        es.SetSelectedGameObject(g);
    }

}
