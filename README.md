Unity Project

When cloned, open the project folder with Unity Version 2017.4.0f1

Genre: Fantasy, JRPG.

Setting:
Hybrid of magic and technology.    20XX.
On made up planet with made up design.

Plot:
Mystical crystals power magic abilities.
Most are stolen by antagonist to become more powerful.
Protagonist must go and get the mystical crystals back.

Mechanics:
Turn based battles.
Overworld maps with NPCs to talk to.
Main and side quests to do.